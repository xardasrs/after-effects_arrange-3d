## Name
Arrange 3D


## Installation & Start

### To DOCK and UNDOCK the Panel in After effects:

1. Copy the file into the folder 
    - "Support Files\Scripts\ScriptUI Panels" (on Windows) 
    or 
    - "Scripts/ScriptUI Panels" (on Mac) 
    
    of your After Effects installation. 
2.	Start the script via the "Window" menu in After Effects



### To use the script without the DOCK-funktion:

1.	Copy the file into the folder 
    - "Support Files\Scripts" (on Windows) 
    or 
    - "Scripts" (on Mac) 
    
    of your After Effects installation. 
2.	Start the script via the "File > Script" menu in After Effects
